import java.net.Socket;
import java.net.InetAddress;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SimpleClient {
	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Usage: SimpleClient <hostname> <port>");
			return;
		}

		// Attempt to open a socket to server
		Socket clientSocket;
		try {
			// Get internet address
			InetAddress serverAddr = InetAddress.getByName(args[0]);
			System.out.println("Connecting to " + args[0] + ":" + args[1]);
			clientSocket = new Socket(serverAddr, Integer.parseInt(args[1]));
		} catch (Exception ex) {
			System.out.println("Failed to find the server");
			return;
		}

		// Try read input stream from the server
		try {
			BufferedReader reader = new BufferedReader(
				new InputStreamReader(clientSocket.getInputStream()));

			// Read all lines in the buffer
			String msg = "";
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.isEmpty()) {
					break;
				}
				msg += line + "\n";
			}

			// Close the read and the socket as we are done with them
			reader.close();
			clientSocket.close();

			System.out.println("Recieved message:");
			System.out.print(msg);
		} catch (Exception ex) {
			System.out.println("Failed to get message from " + args[0]);
		}
	}
}
