import java.net.InetAddress;

public class Resolve {
	public static void main(String args[]) {

		if (args.length == 0)
			System.out.println("Usage: Resolve <name1> <name2> ... <nameN>");

		// For each hostname input
		for (int addr = 0; addr < args.length; addr ++) {
			String address = "";

			// Attempt to fetch address from hostname
			try {
				InetAddress IP = InetAddress.getByName(args[addr]);
				address = IP.getHostAddress();
			} catch (Exception ex) {
				address = "unknown host";
			}
			
			// print result
			System.out.println(args[addr] + " : " + address);
		}
		
	}
}
