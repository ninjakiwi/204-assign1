import java.net.InetAddress;

public class Reverse {
	public static void main(String args[]) {

		if (args.length == 0)
			System.out.println("Usage: Reverse <address1> <address2> ... <addressN>");

		// for each input arg (ip addresses)
		for (int ip = 0; ip < args.length; ip ++) {
			String address = "";

			try {
				// Get hostname from ip
				InetAddress IP = InetAddress.getByName(args[ip]);
				address = IP.getHostName();

				// If hostname is the same, hostname must not have been found
				if (address.equals(args[ip])) {
					address = "no name";
				}
			} catch (Exception ex) {
				address = "no name";
			}
			
			System.out.println(args[ip] + " : " + address);
		}
		
	}
}
