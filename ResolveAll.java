import java.net.InetAddress;

public class ResolveAll {
	public static void main(String args[]) {

		if (args.length == 0)
			System.out.println("Usage: ResolveAll <name1> <name2> ... <nameN>");

		// for each input hostname
		for (int addr = 0; addr < args.length; addr ++) {

			// create empty address list (start with 10 but it can grow)
			InetAddress[] IPs = new InetAddress[0];

			// Attempt to fetxh all ip addresses for the hostname
			try {
				IPs = InetAddress.getAllByName(args[addr]);
			} catch (Exception ex) {
				System.out.println(args[addr] + " : " + "unknown host");
			}
			
			// For each found address
			for (InetAddress IP : IPs) {
				// Check ip was found and print result
				// (shouldn't crash at this point but good to be safe)
				try {
					String address = IP.getHostAddress();
					System.out.println(args[addr] + " : " + address);
				} catch (Exception ex) {
					System.out.println(args[addr] + " : " + "unknown host");
				}
			}
		}
		
	}
}
