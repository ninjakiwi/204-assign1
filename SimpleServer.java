import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetAddress;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;

public class SimpleServer {
	public static void main(String[] args) {
		// Attempt to open a socket
		ServerSocket server;
		try {
			server = new ServerSocket(0);
		} catch (Exception ex) {
			System.out.println("Failed to open port");
			return;
		}

		System.out.println("Ready for client connections");
		System.out.println("Listening on port " + server.getLocalPort());

		// Reply to requesting clients until process killed
		while(true) {
			System.out.println("Waiting for new client...");
			
			// Wait for a client
			Socket clientSocket;
			try {
				// Blocks until connected
				clientSocket = server.accept();
			} catch (Exception ex) {
				System.out.println("A client attempted to connect but failed");
				continue;
			}

			// Get client name
			// If finding host name fails we just get the ip
			InetAddress clientAddr = clientSocket.getInetAddress();
			String clientHostName = clientAddr.getHostName();
			String clientIPAddr = clientAddr.getHostAddress();

			// Attempt to send a message to the client
			try {
				BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(clientSocket.getOutputStream()));

				// Write host and ip addr to the client
				writer.write("Hello, " + clientHostName + "\n" + "Your IP address is " + clientIPAddr + "\n");
				writer.flush();
				writer.close();

				clientSocket.close();

				System.out.println("Message sent to " + clientHostName);

			} catch (Exception ex) {
				System.out.println("Failed to connect and send message to client");
			}
		}
	}
}
